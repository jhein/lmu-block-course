#include <math.h>
#include <limits.h>

static const double g = 0.85; // Parameter in Henyey Greenstein
static const int N_ptc = 100000;
static const double deg2rad = 2.0*M_PI/360.0;

// Setup related parameters
static const double theta_0   = 0.0 * deg2rad; // in rad
static const int do_continuous_absorption = 1;

static const int cell_of_a_cloud = 10;
static const double cloud_k = 1.0; // in 1/km

static const double surface_albedo = 0.0;

// numerical parameters
static const double max_extinction = 1e+9;
static const double min_extinction = 1e-9;
static const double min_angle = 1e-6;
static const double min_dist_corr = 1/max_extinction;
static const int max_interation = INT_MAX;
