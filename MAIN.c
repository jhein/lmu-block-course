#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include "parameters.h"
#include "numerics.h"
#include "scattering.h"
#include "grid.h"
#include "read_tables.h"
#include "surface.h"
#include "local_estimate.h"

int main(){

	// transmittance, reflectance and absorption counters
	double NT = 0, NR = 0, NA = 0;
	double R, e_R, T, e_T, A, e_A;

	int N_direction_output = 10, rad_bin;
	double bounds[N_direction_output+1], rad_min = 0, rad_max = +1;
	double radiance[N_direction_output];

	bounds[0] = -1.0;
	for(int l = 0; l< N_direction_output; l++){
			bounds[l+1] = (rad_max-rad_min) * ((l+1) / (double)N_direction_output) - rad_min;
			radiance[l] = 0.0;
	}


	// Particle related parameters
	double z = 0.0;
	double n[3], n_tmp[3];
	double tau, tau_cloud, ptc_weight;
	int interaction_counter = 0;
	int cell_id = 0;

	// distances
	double d_inte = 0.0; // distance to next interaction (scat or abso)
	double d_cell = 0.0; // distance to the next cell
	double d_cloud = 0.0; // distance to the next interaction with a cloud
	double d_min;
	int what_to_do;

	// Local estimator things
	double theta_det = 50 * deg2rad;
	double n_obs[3]; // direction towards observer
	double prob;
	n_obs[0] = 0;
	n_obs[1] = sin(theta_det);
	n_obs[2] = cos(theta_det);

	double tau_ext_obs; // optical depth towards observer
	double obs_count = 0.0; // local estimate towards observer


	// read profiles from file
	int N_cell, rows, min_columns, max_columns, max_length, _n;
	double *cellzb, *k_sca_b, *k_abs_b;
	double tau_total_abs, tau_total_sca, tau_total_ext, tau_total_ext_inc_clo;
	ASCII_checkfile("./tables/ksca_kabs_lambda320.dat", &rows, &min_columns, &max_columns, &max_length);
	read_3c_file("./tables/ksca_kabs_lambda320.dat", &cellzb, &k_sca_b, &k_abs_b, &_n);
	N_cell = rows-1;
	double delta_z   = cellzb[0]; // larges distance from file

	invert_array(cellzb, N_cell+1);
	invert_array(k_sca_b, N_cell+1);
	invert_array(k_abs_b, N_cell+1);

	// tabulated positions are at cell boundaries
	// tabulated ext/abs/sca coefficients are for the cell above
	double k_abs[N_cell], k_sca[N_cell], k_ext[N_cell], w0[N_cell], cloud_k_sca[N_cell], k_ext_including_cloud[N_cell];
	cut_coeff(k_abs_b, k_abs, N_cell);
	cut_coeff(k_sca_b, k_sca, N_cell);

	make_safe(k_abs, N_cell);
	make_safe(k_sca, N_cell);
	obtain_exct_w0(k_sca, k_abs, k_ext, w0, N_cell);

	add_cloud_layer(cloud_k_sca, N_cell);
	make_safe(cloud_k_sca, N_cell);
	add_arrays(k_ext, cloud_k_sca, k_ext_including_cloud, N_cell);

	printf("Model loaded:\n");
	for (int k = 0; k < N_cell; k++) {
		printf("cell %d boun %lf %lf abs/sca/ext %lf %lf %lf %lf %lf \n", k, cellzb[k], cellzb[k+1], k_abs[k], k_sca[k], k_ext[k], cloud_k_sca[k], (cellzb[k+1] - cellzb[k]) * cloud_k_sca[k]);
	}
	printf("\n");

	tau_total_abs = total_optical_depth(k_abs, cellzb, N_cell);
	tau_total_sca = total_optical_depth(k_sca, cellzb, N_cell);
	tau_total_ext = total_optical_depth(k_ext, cellzb, N_cell);
	tau_total_ext_inc_clo = total_optical_depth(k_ext_including_cloud, cellzb, N_cell);


	// initialize RNG
	srand(2.0);

	// for all particles
	for (int i = 0; i<N_ptc; i++){

		// initialize particle position
		z = delta_z;
		cell_id = get_cell_id(z, cellzb, N_cell);
		ptc_weight = 1.0;

		// and direction
		n[0] = sin(theta_0);
		n[1] = 0.0;
		n[2] = - cos(theta_0);
		interaction_counter = 0;

		// propagte particle
		while(1){

			// Draw distances
			tau = random_tau();
			tau_cloud = random_tau();
			d_cloud = tau_cloud / cloud_k_sca[cell_id];
			if (do_continuous_absorption){
				d_inte = tau / k_sca[cell_id];
			} else {
				d_inte = tau / k_ext[cell_id];
			}
			d_cell = distance_to_next_cell(z, n[2], cell_id, cellzb);
			d_min = fmin(d_inte, d_cell);
			d_min = fmin(d_min, d_cloud);
			//printf("particle %d in cell %d at z=%lf dext/dcell/dcloud %lf %lf %lf\n", i, cell_id, z, d_inte, d_cell, d_cloud);


			// Move particle
			z += n[2] * d_min;
			if (do_continuous_absorption){
				ptc_weight *= exp(-k_abs[cell_id] * d_min) ;
			}

			// particle reaches bottom
			if (z <= 0.0){
				what_to_do = sample_options(surface_albedo);
				if (what_to_do == 0) { //we do have surface reflection
					// particle reflected by surface
					get_new_dir_after_surface_scat(n);
					z = min_dist_corr;
					cell_id = 0;
					n[2] = fabs(n[2]);

					//Local estimator stuff
					tau_ext_obs = get_tau_ext_in_direction(cellzb, k_ext_including_cloud, N_cell, z, n_obs);
					obs_count += ptc_weight * get_local_estimate_in_direction(n_obs, n, tau_ext_obs, -1);
					// printf("-1 At depth z=%lf ta_obs=%lf tau_tot=%lf est = %lf\n", z, tau_ext_obs, tau_total_ext_inc_clo, obs_count);

				} else {
					// particle absorbed by surface
					NT+= ptc_weight;
					break;
				}
			}

			// particle reaches the top
			if (z > delta_z){
				NR+= ptc_weight;
				//printf("particle %d reached the top.\n", i);
				rad_bin = get_cell_id(n[2], bounds, N_direction_output);
				radiance[rad_bin] += ptc_weight;
				break;
			}

			// particle interacts
			if (d_min == d_inte){
				if (do_continuous_absorption) {
						what_to_do = 0;
				} else {
						what_to_do = sample_options(w0[cell_id]);
				}

				if (what_to_do == 0){
					// do a scattering
					do_scattering(n, n_tmp, 0);
					n[0] = n_tmp[0];
					n[1] = n_tmp[1];
					n[2] = n_tmp[2];
					interaction_counter += 1;

					// Do local estimator stuff
					tau_ext_obs = get_tau_ext_in_direction(cellzb, k_ext_including_cloud, N_cell, z, n_obs);
					obs_count += ptc_weight * get_local_estimate_in_direction(n_obs, n, tau_ext_obs, 0);
					// printf("0 At depth z=%lf ta_obs=%lf tau_tot=%lf est = %lf\n", z, tau_ext_obs, tau_total_ext_inc_clo, obs_count);

				} else {
					// do absorption
					//printf("particle %d Was absorbed.\n", i);
					NA+=1;
					break;
				}

			} else if (d_min == d_cloud)  {
				do_scattering(n, n_tmp, 1);
				n[0] = n_tmp[0];
				n[1] = n_tmp[1];
 				n[2] = n_tmp[2];
				tau_ext_obs = get_tau_ext_in_direction(cellzb, k_ext_including_cloud, N_cell, z, n_obs);
				obs_count += ptc_weight * get_local_estimate_in_direction(n_obs, n, tau_ext_obs, 1);
				// printf("1 At depth z=%lf ta_obs=%lf tau_tot=%lf est = %lf\n", z, tau_ext_obs, tau_total_ext_inc_clo, obs_count);

				interaction_counter += 1;
			} else {
				// move particle to the next cell
				cell_id = get_cell_id(z, cellzb, N_cell);
				interaction_counter += 1;
			}


			if (interaction_counter > max_interation){
				printf("particle %d took too many iterations. Killed.\n", i);
				break;
			}

			}
	}

	T =(double)NT /(double)N_ptc;
	e_T = error(NT);

	R =(double)NR /(double)N_ptc;
	e_R = error(NR);

	A =(double)NA /(double)N_ptc;
	e_A = error(NA);

	printf("Parameters:\n");
	printf("Number of ptcs.\t\t %d \n", N_ptc);
	printf("Size of domain \t\t %lf km \n", delta_z);
	printf("Total optical depth (abs) \t %lf \n", tau_total_abs);
	printf("Total optical depth (sca) \t %lf \n", tau_total_sca);
	printf("Total optical depth (ext) \t %lf \n", tau_total_ext);
	printf("Total optical depth (ext + cloud) \t %lf \n", tau_total_ext_inc_clo);
	printf("theta0\t\t\t %lf \n", theta_0);
	printf("\n");
	printf("Reflectance [%%] \t %lf +/- %lf (%lf) \n", 100.*R, 100.*e_R, NR);
	printf("Transmittance [%%] \t %lf +/- %lf (%lf) \n", 100.*T, 100.*e_T, NT);
	if (do_continuous_absorption){
		printf("Absorption [%%] \t\t %lf (continuous) \n", 100.*(1-R-T));
	} else {
		printf("Absorption [%%] \t\t %lf +/- %lf (%lf) \n", 100.*A, 100.*e_A, NA);
	}
	printf("Local estimate %lf\n", obs_count/N_ptc);


	printf("Radiance:\n[");
	for (int l = 0; l<N_direction_output; l++){
		printf("%lf, ", radiance[l]/(double)N_ptc);
	}
	printf("]\n");
	return 0;
}
