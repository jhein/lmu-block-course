#include "parameters.h"
#include "numerics.h"
#include "Rayleigh.h"
#include "Henyey-Greenstein.h"
#include "grid.h"

double get_local_estimate_in_direction(double * n_obs, double * n, double tau_ext, int scat_type){
	/*
	Input : 	n_obs = direction of the observer
						n = particle direction before scattering
						tau_ext = optical depth towards observer
						scat_type as above
	*/
	double delta_mu, p;

	delta_mu =  scalar_prod(n_obs, n); // cos of angle between observer and photon direction

  if (scat_type == -1){
    p = 0.5 * sin(2 * acos(n_obs[2])) ;
  } else if (scat_type == 0){
		// raleigh
		p = p_raleigh(delta_mu);
	} else  {
		// hg
		p = p_HG(delta_mu);
	}
	return p * exp(-tau_ext) / n_obs[2];
}


double get_tau_ext_in_direction(double * cellzb, double * k_ext, int N_cell, double z, double * n_obs){
  double tau_ext = 0.0;
  int cell_id = get_cell_id(z, cellzb, N_cell);

  // calculate vertical optical depth
  tau_ext = (cellzb[cell_id+1] - z) * k_ext[cell_id];

  for (int i = cell_id+1; i < N_cell; i++){
    tau_ext += (cellzb[i+1]-cellzb[i]) * k_ext[i];
  }

  // correct for observer direction
  tau_ext = tau_ext / n_obs[2];
  return tau_ext;
}
