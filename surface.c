#include <math.h>
#include "numerics.h"

void get_new_dir_after_surface_scat(double * n){
	double theta, phi;

	theta = random_theta_diffuse_surface();
	phi = random_phi();

	n[0] = sin(theta)*cos(phi);
	n[1] = sin(theta)*sin(phi);
	n[2] = cos(theta);

	return;
}
