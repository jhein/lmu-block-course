// randomizer functions
double random_phi();
double random_theta_henyey_greenstein();
double random_theta_rayleigh();
double random_theta_diffuse_surface();
double random_tau();
int sample_options(double w0);

// Numerical stuff
double norm2( double a[]);
void cross_product(double a[], double b[], double c[]);
double scalar_prod(double a[], double b[]);
void add_arrays(double * a, double * b, double * c, int len);

// Analysis functions
double error( int N );
