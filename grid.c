#include <stdio.h>
#include <math.h>
#include "parameters.h"

void invert_array(double arr[], int len){
  double _arr[len];
  int i, j;
  for (i = len - 1, j = 0; i >= 0; i--, j++){
    _arr[j] = arr[i];
  }

  for (i = 0; i< len; i++){
    arr[i] = _arr[i];
  }
  return;
}

void make_safe(double * arr, int len){
  for(int i =0; i< len; i++){
    if (arr[i] < min_extinction){
      arr[i] = min_extinction;
    }
    if (arr[i] > max_extinction){
      arr[i] = max_extinction;
    }
  }
  return;
}

int get_cell_id(double z, double *cellzb, int N_cell){
  int i ;
  for(i = 0; i < N_cell; i++){
      if ((cellzb[i] < z)&&(cellzb[i+1] >= z)) {
          break;
      }
  }
  return i;
}

double distance_to_next_cell(double z, double nz, int cell_id, double *cellzb){
  double d_cell = 0.0;

  if (nz < - min_angle){
    // next cell is below particle
    d_cell = (z - cellzb[cell_id]) / fabs(nz) ;
  } else if (nz > min_angle) {
    // next cell is above particle
    d_cell = (cellzb[cell_id+1] - z) / fabs(nz) ;
  } else {
    // particle move horizontally
    d_cell = 1.0e99;
  }

  if (d_cell < min_dist_corr) {
    d_cell = min_dist_corr;
  }

  return d_cell;
}


void obtain_exct_w0(double * k_sca, double * k_abs, double * k_ext, double * w0, int N_cell){
  for (int i = 0; i<N_cell; i++){
    k_ext[i] = k_sca[i] + k_abs[i];
    w0[i] = k_sca[i] / k_ext[i];
  }
  return;
}

void get_center(double * cellz, double * cellzb, int N_cell){

  for (int i = 0; i <N_cell; i++){
    cellz[i] = 0.5*(cellzb[i+1] + cellzb[i]);
  }
  return;
}

double total_optical_depth(double * k_ext, double * cellzb,  int N_cell){
  double tau = 0;
  for (int i = 0; i<N_cell; i++){
    tau += k_ext[i] * (cellzb[i+1] - cellzb[i]);
  }
  return tau;
}

void cut_coeff(double * k_b, double * k, int N_cell){
  for (int i = 0; i<N_cell; i++){
    k[i] = k_b[i];
  }
  return;
}


void add_cloud_layer(double * cloud_k_sca, int N_cell){
  for (int i=0; i<N_cell; i++){
    cloud_k_sca[i] = 0;
    if (i == cell_of_a_cloud){
      cloud_k_sca[i] = cloud_k;
    }
  }
  return;
}
