#include <math.h>

double F_raleigh(double mu){
  return 0.325*mu + 0.125*mu*mu*mu - 0.5;
}

double norm_raleigh(){
  return F_raleigh(1) - F_raleigh(-1);
}

double F_inv_raleigh(double r){
  double q = -8.*r + 4.;
  double D = 1. + q*q /4.;
  double u = cbrt(-q/2. + sqrt(D));
  double mu = u - 1/u;
  return mu;
}

double p_raleigh(double mu){
  double n = norm_raleigh();
  return 4./(3.*n) * (1.+mu*mu);
}
