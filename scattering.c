#include <math.h>
#include <stdio.h>
#include "parameters.h"
#include "Henyey-Greenstein.h"
#include "Rayleigh.h"
#include "numerics.h"

void get_u(double *ninc, double *u){
	// Get first perpendicular vector to ninc
	// case discrimintation
	double unorm;
	if (fabs(ninc[2]) > 0.9) {
		u[0] = ninc[2];
		u[1] = 0.0;
		u[2] = -ninc[0];
  } else {
		u[0] = ninc[1];
		u[1] = -ninc[0];
		u[2] = 0.0;
	}

	// Normalization
	unorm = norm2(u);
	for (int i = 0; i<3; i++){
		u[i] = u[i]/unorm;
	}
	return;
}


void get_v(double *u, double *ninc, double *v){
	// Get second perpendicular vector
	cross_product(ninc, u, v);
	return;
}


void do_scattering(double * ninc, double * nout, int scat_type){
	double theta, phi; // polar and azimuthal angles between incoming and outgoing direction
	double u[3], v[3]; // helping coordinate system

	// Draw new angles theta and phi
	if (scat_type == 0){
		theta = random_theta_rayleigh();
	} else {
		theta = random_theta_henyey_greenstein();
	}

	phi = random_phi();

	// Get u and v
	get_u(ninc, u);
	get_v(u, ninc, v);

	//printf("angles %lf %lf %lf\n", scalar_prod(u, ninc) , scalar_prod(u, v), scalar_prod(ninc, v));

  // Get outgoing direction
  nout[0] = ninc[0]*cos(theta) + u[0]*sin(theta)*cos(phi) + v[0]*sin(theta)*sin(phi);
  nout[1] = ninc[1]*cos(theta) + u[1]*sin(theta)*cos(phi) + v[1]*sin(theta)*sin(phi);
  nout[2] = ninc[2]*cos(theta) + u[2]*sin(theta)*cos(phi) + v[2]*sin(theta)*sin(phi);

	//printf("Checking sampling %lf %lf \n", scalar_prod(ninc, nout), cos(theta));
	return;

}
