#include <math.h>

#include "parameters.h"
#include "Henyey-Greenstein.h"

double F_HG_0( double mu ){
	// Unnormalized cumulative distribution for Henyey-Greenstein
	return (1-g*g)/(g) * 1.0/sqrt(g*g -2*g*mu + 1);
}

double F_HG_m(){
	// evaluate F_HG_0 at mu = -1
	return F_HG_0( -1.0 );
}

double F_HG_p(){
	// evaulate F_HG_0 at +1
	return F_HG_0( +1.0 );
}

double norm(){
	// Normalization constant of F_HG_0
	return F_HG_p() - F_HG_m();
}

double p_HG(double mu){
	// normalized probability distribution
	double n = norm();
	return (1.0-g*g)/((1+g*g-2*g*mu) * n);
}

double F_inv_HG( double r ){
	// Sample the Henyey-Greenstein distribution
	// random number r between 0 and 1
	double Fm = F_HG_m();
	double Fp = F_HG_p();
	double LHS = r * (Fp - Fm) + Fm ;
	return 1/(2.0*g) * ( 1 + g*g - ((1-g*g)/g)*((1-g*g)/g)  * 1/(LHS*LHS) );
}
