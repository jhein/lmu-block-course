
CC=clang # define the compiler to use
TARGET=main # define the name of the executable
SOURCES= read_tables.c Rayleigh.c Henyey-Greenstein.c numerics.c surface.c scattering.c grid.c local_estimate.c MAIN.c
CFLAGS=-Wall
LFLAGS=-lm

# define list of objects
OBJSC=$(SOURCES:.c=.o)
OBJS=$(OBJSC:.cpp=.o)

# the target is obtained linking all .o files
all: $(SOURCES) $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(TARGET) $(LFLAGS)

purge: clean
	rm -f $(TARGET)

clean:
	rm -f *.o .DS_Store
