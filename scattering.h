// Scattering stuff
void get_u(double *ninc, double *u);
void get_v(double *u, double *ninc, double *v);
void do_scattering(double * ninc, double * nout, int scat_type);
