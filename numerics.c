#include <math.h>
#include "parameters.h"
#include "Henyey-Greenstein.h"
#include "Rayleigh.h"
#include <stdlib.h>

double error(int N) {
	return sqrt((double)(N_ptc-N)/((double)N_ptc*(double)N));
	}


double random_phi(){
	double r = rand()/(double)RAND_MAX;
	return 2 * M_PI * r;
}

double random_theta_rayleigh(){
	double r = rand()/(double)RAND_MAX;
	double mu;
	mu = F_inv_raleigh( r );
	return acos(mu);
}

double random_theta_henyey_greenstein(){
	double r = rand()/(double)RAND_MAX;
	double mu;
	mu = F_inv_HG( r );
	return acos(mu);
}

double random_theta_diffuse_surface(){
	double r = rand()/(double)RAND_MAX;
	return asin(sqrt(r));
}

int sample_options(double w0){
	double r = rand()/(double)RAND_MAX;
	if (r < w0){
		return 0;
	} else {
		return 1;
	}
}

double random_tau(){
	double r = rand()/(double)RAND_MAX;
	//printf ("r %lf and -log(r) %lf \n", r, -log(1-r) ) ;
	return -log(1-r);
}

double scalar_prod(double * a, double * b){
	double c = 0;
	c = a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
	return c;
}


void cross_product(double a[], double b[], double c[]) {
	c[0] = a[1] * b[2] - a[2] * b[1];
	c[1] = a[2] * b[0] - a[0] * b[2];
	c[2] = a[0] * b[1] - a[1] * b[0];
}

double norm2( double a[]){
	double result = 0.0;
	for (int i = 0; i<3; i++){
		result += a[i]*a[i];
	}
	result = sqrt(result);
	return result;
}

void add_arrays(double * a, double * b, double * c, int len){
	for(int i = 0; i<len; i++){
		c[i] = a[i] + b[i];
	}
	return;
}
