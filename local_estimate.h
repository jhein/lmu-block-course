double get_local_estimate_in_direction(double * n_obs, double * n, double tau_ext, int scat_type);
double get_tau_ext_in_direction(double * cellzb, double * k_ext, int N_cell, double z, double * n_obs);
