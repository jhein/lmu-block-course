void invert_array(double * arr, int len);
int get_cell_id(double z, double *cellzb, int N_cell);
double distance_to_next_cell(double z, double nz, int cell_id, double *cellzb);
void obtain_exct_w0(double * k_sca, double * k_abs, double * k_ext, double * w0, int N_cell);
void get_center(double * cellz, double * cellzb, int N_cell);
void make_safe(double * arr, int len);
double total_optical_depth(double * k_ext, double * cellzb,  int N_cell);
void cut_coeff(double * k_b, double * k, int N_cell);
void add_cloud_layer(double * cloud_k_sca, int N_cell);
